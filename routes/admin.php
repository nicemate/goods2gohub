<?php


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| This file is where the admin routes should be placed
| and remember all routes defined here are prefixed with /admin automatically
|
*/


Route::get('/', 'DashboardController@index')->name('dashboard');

Route::resource('stores', 'StoreController');
Route::resource('categories', 'CategoryController');
Route::post('categories/upload', 'CategoryController@upload');