<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->text('description');
            $table->text('images');
            $table->integer('price');
            $table->unsignedInteger('subcategory_id');
            $table->foreign('subcategory_id')
                ->references('id')->on('subcategories')->onUpdate('cascade')->onDelete('restrict');
            $table->uuid('user_uuid');
            $table->foreign('user_uuid')
                ->references('uuid')->on('users')->onUpdate('cascade')->onDelete('restrict');
            $table->enum('status', ['pending', 'approved', 'declined']);
            $table->unsignedBigInteger('views')->default(0);
            $table->date('featured')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
