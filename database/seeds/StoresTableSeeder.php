<?php

use Illuminate\Database\Seeder;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use App\Store;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stores = [
            'Phones and Tablets',
            'Fashion',
            'Home and Office',
            'Computing',
            'Cameras and Electronics',
            'Baby, Toys and Kids',
            'Health and Beauty',
            'Automobile',
            'Sports and Fitness',
        ];

        foreach ($stores as $store){
            DB::table('stores')->insert([
                'name' => $store,
                'slug' => SlugService::createSlug(Store::class, 'slug', $store)
            ]);
        }
    }
}
