<?php

use Illuminate\Database\Seeder;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use App\Category;
use App\Subcategory;
use App\Store;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stores = [
            [
                'name' => 'Phones and Tablets',
                'id' => 1,
                'categories' => [
                    [
                        'name' => 'Mobile phones',
                        'id' => 1,
                        'subcategories' => [
                            'Basic Phones', 'Mobile', 'Phone Accessories', 'Smart Phones',
                        ],
                    ],
                    [
                        'name' => 'Tablets',
                        'id' => 2,
                        'subcategories' => ['Tablet Accessories', 'Ipad'],
                    ],
                    [
                        'name' => 'Landline',
                        'id' => 3,
                        'subcategories' => ['Corded', 'Cordless', 'Other landlines'],
                    ],
                ],
            ],
            [
                'name' => 'Fashion',
                'id' => 2,
                'categories' => [
                    [
                        'name' => 'Women’s Clothing',
                        'id' => 4,
                        'subcategories' => ['Beach and Swimmer', 'Coats and jackets', 'Coordinates', 'Corporate wears', 'Dresses', 'Jeans and leggings', 'Jerseys', 'Jumpers and cardigans', 'Playsuits and jumpsuits. Plus sizes', 'Skirts', 'Sleepwear', 'Tops', 'Traditional', 'Trousers', 'shorts and leggings', 'Underwear',],
                    ],
                    [
                        'name' => 'Women’s shoes',
                        'id' => 5,
                        'subcategories' => ['Ballerinas and flat' ,'Casual boots' ,'Clearance sale' ,'Court shoes high heels' ,'Petit sizes' ,'Platform shoes Sandals and slippers' ,'Sports shoes', 'Wedges' ,'Wellies' ,'Women’ssneakers'],
                    ],
                    [
                        'name' => 'Women’s shoes',
                        'id' => 6,
                        'subcategories' => ['Broaches and Bowties', 'Clearance sale', 'Hats and hair', 'accessories', 'Jewelry', 'Scarves', 'Women’s bags', 'Women’s belt'],
                    ],
                ],
            ],
        ];

        foreach ($stores as $store){
            foreach ($store['categories'] as $category){
                DB::table('categories')->insert([
                    'name' => $category['name'],
                    'slug' => SlugService::createSlug(Category::class, 'slug', $category['name']),
                    'image' => '',
                    'store_id' => $store['id'],
                ]);

                foreach ($category['subcategories'] as $subcategory){
                    DB::table('subcategories')->insert([
                        'name' => $subcategory,
                        'slug' => SlugService::createSlug(Subcategory::class, 'slug', $subcategory),
                        'image' => '',
                        'category_id' => $category['id'],
                    ]);
                }
            }
        }
    }
}
