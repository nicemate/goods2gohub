<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Store;
use App\Category;
use App\Subcategory;
use App\Item;

class DashboardController extends Controller
{
    public function __construct()
    {
        ;
    }

    public function index() {
        return admin_view('dashboard',[
            'stores' => Store::count(),
            'categories' => Category::count(),
            'subcategories' => Subcategory::count(),
            'items' => Item::count(),
            'users' => User::count(),
        ]);
    }
}
