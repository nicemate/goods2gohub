<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Store;

class StoreController extends Controller
{
    protected $store;

    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = $this->store->ranked()->paginate(20);

        return admin_view('stores.index', [
            'stores' => $stores
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return admin_view('stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:5',
            'rank' => 'numeric',
        ]);

        $store = $this->store->create([
            'name' => $request->name,
            'rank' => $request->rank,
        ]);

        return back()->withSuccess('Store created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = $this->store->findOrFail($id);
        return admin_view('stores.edit', [
            'store' => $store
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = $this->store->findOrFail($id);

        $this->validate($request, [
            'name' => 'required|min:5',
            'rank' => 'numeric',
        ]);

        $store->update([
            'name' => $request->name,
            'rank' => $request->rank,
        ]);

        return back()->withSuccess('Store updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = $this->store->findOrFail($id);
        if($store->admin){
            session()->flash('error', 'Failed! You need to remove store\'s admin right first');
        }
        else{
            $store->delete();
            session()->flash('success', 'Store deleted successfully');
        }
        return response('reload');
    }
}
