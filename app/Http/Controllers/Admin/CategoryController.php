<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Category;
use App\File;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->category->paginate(20);

        return admin_view('categories.index', [
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return admin_view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:5',
            'store_id' => 'required',
            'image' => 'required|single_image',
        ]);

        $category = $this->category->create([
            'name' => $request->name,
            'store_id' => $request->store_id,
            'image' => $request->image,
        ]);

        return back()->withSuccess('Category created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->findOrFail($id);
        $image = File::findOrFail($category->image);
        $pos = strrpos($image->link, '.');
        $ext = strtolower(substr($image->link, ++$pos));

        return admin_view('categories.edit', [
            'category' => $category,
            '_file' => json_encode([
                '_id' => $image->id,
                'name' => $image->name,
                'size' => $image->size,
                'type' => $image->type.'/'.$ext,
                'file' => image_path('categories/'.$image->link),
                'url' => image('categories/'.$image->link)
            ]),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $this->category->findOrFail($id);

        $this->validate($request, [
            'name' => 'required|min:5',
            'store_id' => 'required',
        ]);

        $category->update([
            'name' => $request->name,
            'store_id' => $request->store_id,
        ]);

        return back()->withSuccess('Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->category->findOrFail($id);
        if($category->admin){
            session()->flash('error', 'Failed! You need to remove category\'s admin right first');
        }
        else{
            $category->delete();
            session()->flash('success', 'Category deleted successfully');
        }
        return response('reload');
    }

    /**
     * Upload images via js plugin
     * @param Request $request
     * @return array
     */
    public function upload(Request $request) {
        $this->validate($request, [
            'image' => 'required'
        ]);

        $file = $request->file('image')[0];

        $validator = Validator::make(['image' => $file], ['image' => 'image|max:4300']);

        if($validator->fails()) {
            return abort(403, 'validation fails');
        }

        $path = '/uploads/images/categories';

        $name = str_random(20);

        $original_name = $file->getClientOriginalName();

        // Extract the extension
        $pos = strrpos($original_name, '.');
        $ext = substr($original_name, $pos);

        $full_name = $name.$ext;

        // save original
        Storage::put("{$path}/{$full_name}", file_get_contents($file->getRealPath()));

        // store file
        $file_ = File::create([
            'name' => $original_name,
            'link' => $full_name,
            'path' => $request->getUriForPath($path),
            'size' => $file->getClientSize(),
            'type' => 'image'
        ]);

        return ['file_id' => $file_->getAttribute('id')];
    }
}
