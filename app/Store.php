<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Store extends Model
{
    protected $guarded = ['id'];

    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function categories() {
        return $this->hasMany(Category::class);
    }

    public function scopeRanked($query) {
        return $query->orderBy('stores.rank', 'desc');
    }

    public function scopeTop($query) {
        return $query->where('stores.rank', '>', 0);
    }
}
