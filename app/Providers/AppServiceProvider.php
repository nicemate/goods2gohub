<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use App\Store;
use App\File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // check if file sent exists
        Validator::extend('single_image', function($attribute, $value, $parameters, $validator) {
            if($value) {
                $image = File::findOrFail($value);
                if($image) {
                    return true;
                }
            }
            return false;
        });


        // Share between all Views
        if(!$this->app->runningInConsole())
        {
            $stores = Store::Ranked();
            View::share(['stores' => $stores->get(),'rankedStores' => $stores->top()->get()->take(4)]);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
