<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contact Info
    |--------------------------------------------------------------------------
    |
    |
    |
    */

    'info_email' => env('INFO_EMAIL', 'info@goods2gohub.com'),
    'phone' => env('INFO_PHONE', '+234-(0)-815-255-4732'),
];
