@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <section class="page-header">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>

                <li class="active">About Us</li>
            </ul>
        </div>
    </section>

    <div class="container about-container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="word-rotator-title">
                    We Value
                    <strong>
                        <span class="word-rotate" data-plugin-options="{'delay': 2000}">
                            <span class="word-rotate-items">
                                <span>Service.</span>
                                <span>Satisfaction.</span>
                                <span>Innovation.</span>
                            </span>
                        </span>
                    </strong>
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr class="medium">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h3 class="h1 heading-primary"><strong>Who</strong> We Are</h3>
                <p>
                    Goods2go Hub is an Online Shopping Mall in Nigeria owned by Goods2go Concepts – a company that provides online market place for users to offer, sell and buy just about anything in a variety of valuing layouts and locations. The concrete contract for sale is right between the seller and buyer.
                </p>
                <p>
                    Goods2go Hub is focused on express shopping that allows you to place your order and get your order expressly delivered to you few hours from your online purchase within a stipulated time and designated location as specified on the site. From our online market place, you can purchase all your electronics, mobile phones, computers, fashion, beauty products, home and kitchen, books, home appliances, kiddies, fashion items for men, women, and children; cool gadgets, computers, groceries, automobile parts, Generator and Power solutions, Building and construction materials and more on the go. An experience with Goods2go Hub online store is one that entails Shopping online with great ease and excellent speed.
                </p>
                <p>
                    Whatever it is you may wish to buy, Goods2go Hub offers more at prices which you can trust as you get so much value for your money and lots more - irrespective of your taste, class, and preferences. On Goods2go Hub you get to make your payment on delivery conveniently as you shop expressly or choose any of our other online payment methods. We make shopping easy and fast and secure in Nigeria as we initiated our express operations in FCT – Abuja and its environs; the country’s Capital. We provide you with a wide range of products you can trust, and the best prices on this wide range of products. Always set out in style with all brands of Fashion and Style as we bring you awesome fashion collections from top brands no matter what your style could be; cooperate wears, casual, Nigerian Native attires and fabrics, footwear, accessories, classy men’s and women's shoes from top designers, watches, with cosmetics and skin care products and much more – all available on Goods2go Hub online shopping mall. Goods2go Hub makes online shopping fast and exciting so start your shopping experience now.
                </p>
            </div>

            <div class="col-md-6">
                <h3 class="h1 heading-primary"><strong>Express</strong> Shopping Experience</h3>
                <p>
                    Goods2go Hub is your Nigeria’s online shopping last stop – it’s a place to shop till you drop; we call it a sweet haven where you could possibly encounter all that you need for life and living at the best deals and prices. Some of our standard categories include all made in Naija products, electronics, mobile phones, computers, fashion, beauty products, home and kitchen, Building and construction materials, automotive and industrial, books, musical equipment, babies’ and kids’ items, sports and fitness, and a whole lot more from the finest brands.
                </p>
                <p>
                    There are also added special services for the sellers and consumers, gift vouchers and discounts, in other to ensure the best and professional transaction across different categories to deliver to you an express and unforgettable online shopping experience.
                </p>
                <p>
                    Also benefit from free shipping rates for certain products and locations as indicated on our sites and with the bulk purchase option, you can enjoy low shipping rates, discounted prices and flexible payment. Shopping on Goods2go Hub, you can pay with PayPal, your debit card, or pay on delivery; any of these at your very own choice and convenience.
                </p>
                <p>
                    Afford the best of your standard of living online right at the comfort of your home. This opportunity is here for you, hurry up now; shop and keep smiling as you will be getting the best prices on some of the best-selling and quality brands in Africa – what have you. Hence, An experience with Goods2go Hub online store is one that entails Shopping online with great ease and excellent speed.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr class="medium">
            </div>
        </div>

    </div>
@endsection