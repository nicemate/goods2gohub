@extends('layouts.app')

@section('content')
<div class="container">
    <section class="form-section register-form">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <h1 class="h2 heading-primary font-weight-normal mb-md mt-xlg">Create an Account</h1>

                    <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                        <div class="box-content">
                            <form class="" role="form" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <h4 class="heading-primary text-uppercase mb-lg">PERSONAL INFORMATION</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                            <label for="firstName" class="font-weight-normal">First Name <span class="required">*</span></label>
                                            <input id="firstName" type="text" class="form-control" name="firstName" value="{{ old('firstName') }}" required autofocus>
                                            @if ($errors->has('firstName'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                            <label for="lastName" class="font-weight-normal">Last Name <span class="required">*</span></label>
                                            <input id="lastName" type="text" class="form-control" name="lastName" value="{{ old('lastName') }}" required autofocus>
                                            @if ($errors->has('lastName'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="font-weight-normal">Email Address <span class="required">*</span></label>
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="checkbox mb-xlg">
                                            <label>
                                                <input id="subscribe" type="checkbox" name="subscribe" value="{{ old('subscribe') }}" value="1">
                                                Sign Up for Newsletter
                                            </label>
                                        </div>

                                        <h4 class="heading-primary text-uppercase mb-lg">LOGIN INFORMATION</h4>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('phoneNumber') ? ' has-error' : '' }}">
                                            <label for="phoneNumber" class="font-weight-normal">Phone Number <span class="required">*</span></label>
                                            <input id="phoneNumber" type="text" class="form-control" name="phoneNumber" value="{{ old('phoneNumber') }}" required>
                                            @if ($errors->has('phoneNumber'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('phoneNumber') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="font-weight-normal">Password <span class="required">*</span></label>
                                            <input id="password" type="password" class="form-control" name="password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password-confirm" class="font-weight-normal">Confirm Password <span class="required">*</span></label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="required mt-lg mb-none">* Required Fields</p>

                                        <div class="form-action clearfix mt-none">
                                            <a href="{{ route('login') }}" class="pull-left"><i class="fa fa-angle-double-left"></i> Back</a>

                                            <input type="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
