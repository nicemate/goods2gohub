@extends('layouts.app')

@section('content')
    <section class="form-section">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-4">
                    <h1 class="h2 heading-primary font-weight-normal mb-md mt-xlg"></h1>
                    <div class="">
                        <h3 class="heading-text-color font-weight-normal">New Customers</h3>
                        <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                    </div>

                    <div class="clearfix">
                        <a href="{{ route('register') }}" class="btn btn-primary">Create an Account</a>
                    </div>
                </div>

                <div class="col-md-4">
                    <h1 class="h2 heading-primary font-weight-normal mb-md mt-xlg">Login</h1>
                    <div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
                        <div class="box-content">
                            <form class="" role="form" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-content">
                                    <h3 class="heading-text-color font-weight-normal">Registered Customers</h3>
                                    <p>If you have an account with us, please log in.</p>
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="font-weight-normal">Email Address <span class="required">*</span></label>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="font-weight-normal">Password <span class="required">*</span></label>
                                        <input id="password" type="password" class="form-control" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-action clearfix">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                                    <p class="required">* Required Fields</p>
                                    <a href="#" class="pull-left">Forgot Your Password?</a>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
