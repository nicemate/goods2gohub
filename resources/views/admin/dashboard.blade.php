@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="row">
            <div class="col-md-3">
                <section class="panel panel-featured-left panel-featured-tertiary">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Stores</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $stores }}</strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a href="{{admin_route('stores.index')}}" class="text-muted text-uppercase">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-3">
                <section class="panel panel-featured-left panel-featured-tertiary">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fa fa-cubes"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Categories</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $categories }}</strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a href="{{admin_route('categories.index')}}" class="text-muted text-uppercase">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-3">
                <section class="panel panel-featured-left panel-featured-tertiary">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fa fa-tree"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Subcategories</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $subcategories }}</strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-3">
                <section class="panel panel-featured-left panel-featured-tertiary">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fa fa-barcode"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Items</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $items }}</strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Other Stats</h2>
            </header>
            <div class="panel-body">
                <div class="row text-center">
                    <div class="col-md-2">
                        <div class="h4 text-bold mb-none">{{ $users }}</div>
                        <p class="text-xs text-muted mb-none">Registered Profiles</p>
                    </div>
                    <div class="col-md-2">
                        <div class="h4 text-bold mb-none">488</div>
                        <p class="text-xs text-muted mb-none">Placed Orders</p>
                    </div>
                    <div class="col-md-2">
                        <div class="h4 text-bold mb-none text-center">488</div>
                        <p class="text-xs text-muted mb-none">Average Profile Visits</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- end: page -->
@endsection