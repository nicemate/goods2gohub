@extends('admin.layouts.app')

@section('title', 'Categories')

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Categories Listing</h2>
                    <div class="panel-actions">
                        <a href="{{ admin_route('categories.create') }}">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-primary pull-right"><i class="fa fa-plus"></i> New Store</button>
                        </a>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-none">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Store</th>
                                <th>Subcategories</th>
                                <th>Created</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $key => $category)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->store->name }}</td>
                                    <td>{{ $category->subcategories()->count() }}</td>
                                    <td>{{ $category->created_at}}</td>
                                    <td class="actions-hover actions-fade">
                                        <a href="{{ admin_route('categories.edit', $category->id) }}"><i class="fa fa-pencil"></i></a>
                                        <a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
@endsection