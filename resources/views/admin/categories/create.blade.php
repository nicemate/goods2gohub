@extends('admin.layouts.app')

@section('title', 'New Category')

@push('styles')
    <link href="/plugins/fileuploader/src/jquery.fileuploader.css" type="text/css" rel="stylesheet" />
    <link href="/plugins/fileuploader/src/jquery.fileuploader-theme-dragdrop.css" type="text/css" rel="stylesheet" />
@endpush

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">New Category</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" role="form" method="POST" action="{{ admin_route('categories.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-3 control-label" class="font-weight-normal">Name <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}" required>
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('rank') ? ' has-error' : '' }}">
                            <label for="rank" class="col-md-3 control-label" class="font-weight-normal">Store</label>
                            <div class="col-md-6">
                                <select id="store_id" name="store_id" data-plugin-selectTwo class="form-control populate" placeholder="Select">
                                    <option value="">Select One</option>
                                    @foreach(\App\Store::ranked()->get() as $store)
                                        <option value="{{ $store->id }}" {{ old('name') == $store->name ? 'selected' : '' }}>{{ $store->name }}</option>
                                    @endforeach

                                </select>
                            </div>
                            @if ($errors->has('rank'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('rank') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                            <div class="col-md-offset-2 col-md-8">
                                <input id="cat_image" name="image" type="file" class="form-control">
                                <input id="image" name="image" type="hidden" class="form-control" value="{{ old('image') }}" required>
                            </div>
                            @if ($errors->has('image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="divider"> </div>
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
@endsection

@push('scripts')
    <script src="/plugins/fileuploader/src/jquery.fileuploader.js"></script>
    @include('_partials/image_upload')
@endpush