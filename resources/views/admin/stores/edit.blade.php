@extends('admin.layouts.app')

@section('title', 'Edit Store')

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">{{ $store->name }}</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" role="form" method="POST" action="{{ admin_route('stores.update', $store->id) }}>
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-3 control-label" class="font-weight-normal">Name <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input id="name" name="name" type="text" class="form-control" value="{{ old('name') ?: $store->name }}" required>
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('rank') ? ' has-error' : '' }}">
                            <label for="rank" class="col-md-3 control-label" class="font-weight-normal">Rank</label>
                            <div class="col-md-6">
                                <input id="rank" name="rank" type="number" class="form-control" value="{{ old('rank') ?: $store->rank }}" required>
                            </div>
                            @if ($errors->has('rank'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('rank') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="divider"> </div>
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
@endsection