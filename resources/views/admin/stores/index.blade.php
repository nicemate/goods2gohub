@extends('admin.layouts.app')

@section('title', 'Stores')

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Stores Listing</h2>
                    <div class="panel-actions">
                        <a href="{{ admin_route('stores.create') }}">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-primary pull-right"><i class="fa fa-plus"></i> New Store</button>
                        </a>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-none">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Categories</th>
                                <th>Rank</th>
                                <th>Created</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stores as $key => $store)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $store->name }}</td>
                                    <td>{{ $store->categories->count() }}</td>
                                    <td>{{ $store->rank}}</td>
                                    <td>{{ $store->created_at}}</td>
                                    <td class="actions-hover actions-fade">
                                        <a href="{{ admin_route('stores.edit', $store->id) }}"><i class="fa fa-pencil"></i></a>
                                        <a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- end: page -->
@endsection