<!doctype html>
<html class="fixed sidebar-left-collapsed">
<head>

	<!-- Basic -->
	<meta charset="UTF-8">

	<title>@yield('title') | Goods2GoHub Admin</title>
	<meta name="keywords" content="Goods2GoHub Admin" />
	<meta name="description" content="Goods2GoHub Admin">
	<meta name="author" content="djade.net">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="/admin_assets/vendor/bootstrap/css/bootstrap.css" />
	<link rel="stylesheet" href="/admin_assets/vendor/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="/admin_assets/vendor/magnific-popup/magnific-popup.css" />
	<link rel="stylesheet" href="/admin_assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="/admin_assets/vendor/pnotify/pnotify.custom.css" />
	<link rel="stylesheet" href="/admin_assets/vendor/select2/select2.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="/admin_assets/stylesheets/theme.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="/admin_assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="/admin_assets/stylesheets/theme-custom.css">

	<!-- Head Libs -->
	<script src="/admin_assets/vendor/modernizr/modernizr.js"></script>

	@stack('styles')

</head>
<body>
<section class="body">

	<!-- start: header -->
	<header class="header">
		<div class="logo-container">
			<a href="../../layouts" class="logo">
				<img src="/admin_assets/images/logo.png" height="35" alt="Porto Admin" />
			</a>
			<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
				<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
			</div>
		</div>

		<!-- start: search & user box -->
		<div class="header-right">
			<div id="userbox" class="userbox">
				<a href="#" data-toggle="dropdown">
					<figure class="profile-picture">
						<img src="/admin_assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="/admin_assets/images/!logged-user.jpg" />
					</figure>
					<div class="profile-info" data-lock-name="John Doe" data-lock-email="{{ Auth::user()->email }}">
						<span class="name">{{ Auth::user()->firstName }}</span>
						<span class="role">administrator</span>
					</div>

					<i class="fa custom-caret"></i>
				</a>

				<div class="dropdown-menu">
					<ul class="list-unstyled">
						<li class="divider"></li>
						<li>
							<a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> My Profile</a>
						</li>
						<li>
							<a role="menuitem" tabindex="-1" href="{{ route('logout') }}" class="inner-link"
							   onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
								<i class="fa fa-power-off"></i> Logout
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end: search & user box -->
	</header>
	<!-- end: header -->

	<div class="inner-wrapper">
		<!-- start: sidebar -->
		<aside id="sidebar-left" class="sidebar-left">

			<div class="sidebar-header">
				<div class="sidebar-title">
					Navigation
				</div>
				<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
					<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				</div>
			</div>

			<div class="nano">
				<div class="nano-content">
					<nav id="menu" class="nav-main" role="navigation">
						<ul class="nav nav-main">
							<li class="nav-active">
								<a href="{{ admin_route('dashboard') }}">
									<i class="fa fa-home" aria-hidden="true"></i>
									<span>Dashboard</span>
								</a>
							</li>

							<li class="">
								<a href="{{ admin_route('stores.index') }}">
									<i class="fa fa-shopping-cart" aria-hidden="true"></i>
									<span>Stores</span>
								</a>
							</li>

							<li class="">
								<a href="{{ admin_route('categories.index') }}">
									<i class="fa fa-cubes" aria-hidden="true"></i>
									<span>Categories</span>
								</a>
							</li>

							<li class="">
								<a href="index.html">
									<i class="fa fa-tree" aria-hidden="true"></i>
									<span>Subcategories</span>
								</a>
							</li>

							<li class="">
								<a href="index.html">
									<i class="fa fa-barcode" aria-hidden="true"></i>
									<span>Items</span>
								</a>
							</li>
						</ul>
					</nav>

					<hr class="separator" />

				</div>

			</div>

		</aside>
		<!-- end: sidebar -->

		<section role="main" class="content-body">
			<header class="page-header">
				<h2>@yield('title')</h2>
			</header>

			<!-- start: page -->
			@yield('content')
			<!-- end: page -->
		</section>
	</div>


	<!-- Vendor -->
	<script src="/admin_assets/vendor/jquery/jquery.js"></script>
	<script src="/admin_assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
	<script src="/admin_assets/vendor/bootstrap/js/bootstrap.js"></script>
	<script src="/admin_assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="/admin_assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/admin_assets/vendor/magnific-popup/magnific-popup.js"></script>
	<script src="/admin_assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

	<!-- Specific Page Vendor -->
	<script src="/admin_assets/vendor/pnotify/pnotify.custom.js"></script>
	<script src="/admin_assets/vendor/select2/select2.js"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="/admin_assets/javascripts/theme.js"></script>

	<!-- Specific Page Vendor -->
	<script src="/admin_assets/vendor/pnotify/pnotify.custom.js"></script>

	<!-- Theme Custom -->
	<script src="/admin_assets/javascripts/theme.custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="/admin_assets/javascripts/theme.init.js"></script>

	<script>
		$(document).ready(function () {
			@if(Session::has('success') || Session::has('error'))
				var message = "{{Session::get('success') ?: Session::get('error')}}";
				var type = "{{Session::get('success') ? 'success' : 'error'}}";
				console.log(message);
				/*
				 Click to close notifications
				 */
				var notice = new PNotify({
					title: 'Click to Close',
					text: message,
					type: type,
					addclass: 'click-2-close',
					hide: false,
					buttons: {
						closer: false,
						sticker: false
					}
				});

				notice.get().click(function() {
					notice.remove();
				});
			@endif
        });
	</script>

	<!-- start: page scripts -->
	@stack('scripts')
	<!-- end: page scripts -->
</section>
</body>
</html>