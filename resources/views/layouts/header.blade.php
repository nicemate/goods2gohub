<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 147, 'stickySetTop': '-147px', 'stickyChangeLogo': false}">
    <div class="header-body">
        <div class="header-top">
            <div class="container">
                <div class="top-menu-area">
                    <a href="#">Links <i class="fa fa-caret-down"></i></a>
                    <ul class="top-menu">
                        <li><a href="#">Become a Seller</a></li>
                        <li><a href="#">Blog</a></li>
                        <li>
                        @if(Auth::check())
                            <a href="{{ route('logout') }}" class="inner-link"
                               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @else
                            <a href="{{ route('login') }}">Log in / Register</a></li>
                        @endif
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-logo">
                        <a href="/">
                            <img alt="Porto" width="111" height="51" src="/img/logo-shop-red.png">
                        </a>
                    </div>
                </div>
                <div class="header-column">
                    <div class="row">
                        <div class="cart-area">
                            <div class="custom-block">
                                @if(Auth::check())
                                    <span>Hi {{ Auth::user()->firstName }}</span>
                                @else
                                    <span>Hi Awesome Guest</span>
                                @endif
                                <span class="split"></span>
                            </div>

                            <div class="cart-dropdown">
                                <a href="#" class="cart-dropdown-icon">
                                    <i class="minicart-icon"></i>
                                    <span class="cart-info">
                                        <span class="cart-qty">2</span>
                                        <span class="cart-text">item(s)</span>
                                    </span>
                                </a>
                            </div>
                        </div>

                        <div class="header-search">
                            <a href="#" class="search-toggle"><i class="fa fa-search"></i></a>
                            <form action="#" method="get">
                                <div class="header-search-wrapper">
                                    <input type="text" class="form-control" name="q" id="q" placeholder="Search..." required>
                                    <select id="cat" name="cat">
                                        <option value="">All Categories</option>
                                        @foreach($stores as $store)
                                            <option value="{{ $store->slug }}">{{ $store->name }}</option>
                                        @endforeach
                                    </select>
                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>

                        <a href="#" class="mmenu-toggle-btn" title="Toggle menu">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-container header-nav">
            <div class="container">
                <div class="header-nav-main">
                    <nav>
                        <ul class="nav nav-pills" id="mainNav">
                            @foreach($rankedStores as $store)
                            <li class="dropdown dropdown-mega">
                                <a href="#" class="dropdown-toggle">
                                    {{ $store->name }}
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="dropdown-mega-content">
                                            {{--<div class="row">--}}
                                                {{--<div class="dropdown-mega-top">--}}
                                                    {{--<span>Suggestions:</span>--}}
                                                    {{--@foreach($store->categories->take(4) as $category)--}}
                                                        {{--<a href="#">{{ $category->name }}</a>--}}
                                                    {{--@endforeach--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="mega-column row">
                                                <div class="block1 col-md-8">
                                                    <div class="row">
                                                        <ul>
                                                            @foreach($store->categories as $category)
                                                                <div class="col-md-4 col-sm-4">
                                                                    {{--<a href="#" class="cat-img"><img src="../img/demos/shop/cat-tv.png" alt="{{ $category->name }}"></a>--}}
                                                                    <a href="#" class="dropdown-mega-sub-title">{{ $category->name }}</a>
                                                                    <ul class="dropdown-mega-sub-nav">
                                                                        @foreach($category->subcategories as $subcategories)
                                                                            <li><a href="#">{{ $subcategories->name }}</a></li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="right-mega-block col-sm-4">
                                                    {{--<a href="#"><img style="max-width: 100%;padding-right:5px;float:right;" src="../../media/wysiwyg/porto/megamenu/menu-banner8.jpg" alt=""> </a>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            @endforeach

                            <?php $otherStores = $stores->take($rankedStores->count()-$stores->count()); ?>

                            <li class="dropdown dropdown-mega">
                                <a href="#" class="dropdown-toggle">
                                    Other Stores <span class="tip tip-new">{{  $otherStores->count()}}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="dropdown-mega-content dropdown-mega-content">
                                            @foreach($otherStores->chunk(3) as $chunk)
                                                <div class="row">
                                                    @foreach($chunk as $store)
                                                        <div class="col-md-4">
                                                            <a href="#" class="dropdown-mega-sub-title">{{ $store->name }}</a>
                                                            <ul class="dropdown-mega-sub-nav">
                                                                @foreach($store->categories as $category)
                                                                <li><a href="#">{{ $category->name}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="pull-right ">
                                <a href="#">
                                    Become a Seller!  <span class="tip tip-hot">New!</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="mobile-nav">
    <div class="mobile-nav-wrapper">
        <ul class="mobile-side-menu">
            <li><a href="#">Home</a></li>
            <li>
                <span class="mmenu-toggle"></span>
                <a href="#">Fashion <span class="tip tip-new">New</span></a>

                <ul>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Women</a>
                        <ul>
                            <li>
                                <a href="#">Tops &amp; Blouses</a>
                            </li>
                            <li>
                                <a href="#">Accessories</a>
                            </li>
                            <li>
                                <a href="#">Dresses &amp; Skirts</a>
                            </li>
                            <li>
                                <a href="#">Shoes &amp; Boots</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Men</a>

                        <ul>
                            <li>
                                <a href="#">Accessories</a>
                            </li>
                            <li>
                                <a href="#">Watch &amp; Fashion <span class="tip tip-new">New</span></a>
                            </li>
                            <li>
                                <a href="#">Tees, Knits &amp; Polos</a>
                            </li>
                            <li>
                                <a href="#">Pants &amp; Denim</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Jewellery <span class="tip tip-hot">Hot</span></a>

                        <ul>
                            <li>
                                <a href="#">Sweaters</a>
                            </li>
                            <li>
                                <a href="#">Heels &amp; Sandals</a>
                            </li>
                            <li>
                                <a href="#">Jeans &amp; Shorts</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Kids Fashion</a>

                        <ul>
                            <li>
                                <a href="#">Casual Shoes</a>
                            </li>
                            <li>
                                <a href="#">Spring &amp; Autumn</a>
                            </li>
                            <li>
                                <a href="#">Winter Sneakers</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <span class="mmenu-toggle"></span>
                <a href="#">Pages <span class="tip tip-hot">Hot!</span></a>

                <ul>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Category</a>
                        <ul>
                            <li>
                                <a href="demo-shop-8-category-2col.html">2 Columns</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-3col.html">3 Columns</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-4col.html">4 Columns</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-5col.html">5 Columns</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-6col.html">6 Columns</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-7col.html">7 Columns</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-8col.html">8 Columns</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-right-sidebar.html">Right Sidebar</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-list.html">Category List</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Category Banners</a>
                        <ul>
                            <li>
                                <a href="demo-shop-8-category-banner-boxed-slider.html">Boxed slider</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-banner-boxed-image.html">Boxed Image</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-category-banner-fullwidth.html">Fullwidth</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Product Details</a>
                        <ul>
                            <li>
                                <a href="demo-shop-8-product-details.html">Product Details 1</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-product-details2.html">Product Details 2</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-product-details3.html">Product Details 3</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-product-details4.html">Product Details 4</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="demo-shop-8-cart.html">Shopping Cart</a>
                    </li>
                    <li>
                        <a href="demo-shop-8-checkout.html">Checkout</a>
                    </li>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Loign &amp; Register</a>
                        <ul>
                            <li>
                                <a href="demo-shop-8-login.html">Login</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-register.html">Register</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span class="mmenu-toggle"></span>
                        <a href="#">Dashboard</a>
                        <ul>
                            <li>
                                <a href="demo-shop-8-dashboard.html">Dashboard</a>
                            </li>
                            <li>
                                <a href="demo-shop-8-myaccount.html">My Account</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="demo-shop-8-about-us.html">About Us</a>
            </li>
            <li>
                <span class="mmenu-toggle"></span>
                <a href="#">Blog</a>
                <ul>
                    <li><a href="#">Blog</a></li>
                    <li><a href="demo-shop-8-blog-post.html">Blog Post</a></li>
                </ul>
            </li>
            <li>
                <a href="demo-shop-8-contact-us.html">Contact Us</a>
            </li>
            <li>
                <a href="#">Buy Porto!</a>
            </li>
        </ul>
    </div>
</div>