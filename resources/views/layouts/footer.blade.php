<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4>Goods2GoHub</h4>
                <ul class="links">
                    <li>
                        <i class="fa fa-caret-right text-color-primary"></i>
                        <a href="/about">About Us</a>
                    </li>
                    <li>
                        <i class="fa fa-caret-right text-color-primary"></i>
                        <a href="#">Become a Vendor</a>
                    </li>
                    <li>
                        <i class="fa fa-caret-right text-color-primary"></i>
                        <a href="#">Return Policy</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4>My Account</h4>
                <ul class="links">
                    <li>
                        <i class="fa fa-caret-right text-color-primary"></i>
                        <a href="#">My Profile</a>
                    </li>
                    <li>
                        <i class="fa fa-caret-right text-color-primary"></i>
                        <a href="#">My Orders</a>
                    </li>
                    <li>
                        <i class="fa fa-caret-right text-color-primary"></i>
                        <a href="#">My Cart</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="contact-details">
                    <h4>Get in Touch</h4>
                    <ul class="contact">
                        <li><p><i class="fa fa-phone"></i> <strong>Phone:</strong><br> {{ config('contacts.phone') }}</p></li>
                        <li><p><i class="fa fa-envelope-o"></i> <strong>Email:</strong><br> <a href="mailto:{{ config('contacts.info_email') }}">{{ config('contacts.info_email') }}</a></p></li>
                    </ul>
                    <ul class="social-icons">
                        <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="newsletter">
                    <h4>Be the First to Know</h4>
                    <div class="alert alert-success hidden" id="newsletterSuccess">
                        <strong>Success!</strong> You've been added to our email list.
                    </div>

                    <div class="alert alert-danger hidden" id="newsletterError"></div>

                    <p>Enter your e-mail Address:</p>
                    <form id="newsletterForm" action="php/subscribe.php" method="POST">
                        <div class="input-group">
                            <input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <p class="copyright-text">Copyright © {{ config('app.name') }} {{ \Carbon\Carbon::now()->year }}. All Rights Reserved.</p>
    </div>
</footer>