@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <div class="banners-container">
        <div class="container">
            <div class="row">
                <div class="slider-area">
                    <div class="owl-carousel owl-theme owl-bottom-narrow owl-banner-carousel" data-plugin-options="{'items':1, 'loop': false}">
                        <a href="#" class="banner">
                            <img src="/img/slides/slide1.jpg" alt="Banner">
                        </a>
                    </div>
                </div>

                <div class="side-area">
                    <div class="row">
                        <div class="col-md-12 col-sm-4 col-xs-12">
                            <a href="#" class="banner">
                                <img src="/img/banners/banner1.jpg" alt="Banner">
                            </a>
                        </div>

                        <div class="col-md-12 col-sm-4 col-xs-12">
                            <a href="#" class="banner">
                                <img src="/img/banners/banner2.jpg" alt="Banner">
                            </a>
                        </div>

                        <div class="col-md-12 col-sm-4 col-xs-12">
                            <a href="#" class="banner">
                                <img src="/img/banners/banner3.jpg" alt="Banner">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="homepage-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <i class="fa fa-truck bar-icon"></i>
                    <div class="bar-textarea">
                        <h3>FREE SHIPPING &amp; RETURN</h3>
                        <p>Free shipping on all orders over $99.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <i class="fa fa-dollar bar-icon"></i>
                    <div class="bar-textarea">
                        <h3>MONEY BACK GUARANTEE</h3>
                        <p>100% money back guarantee.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <i class="fa fa-support bar-icon"></i>
                    <div class="bar-textarea">
                        <h3>ONLINE SUPPORT 24/7</h3>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="mt-sm mb-sm">
            <div class="row shop-grid">
                <div class="col-md-2">
                    <div class="custom-block sidebar">
                        <h3 class="title">Fashion</h3>
                        <ul>
                            <li><a href="#">Basic Phones</a></li>
                            <li><a href="#">Mobile</a></li>
                            <li><a href="#">Phone Accessories</a></li>
                            <li><a href="#">Smart Phones</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-sm-8">
                            <a href="#" class="banner">
                                <img src="/img/store-banners/full.jpg" alt="Banner">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="#" class="banner category">
                                <img src="/img/store-banners/shirt.jpg" alt="Banner">
                            </a>
                            <a href="#" class="banner category">
                                <img src="/img/store-banners/bag.jpg" alt="Banner">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <hr class="medium">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h3 class="h1 heading-primary"><strong>Express</strong> Shopping Experience</h3>
                <p>
                    Goods2go Hub is your Nigeria’s online shopping last stop – it’s a place to shop till you drop; we call it a sweet haven where you could possibly encounter all that you need for life and living at the best deals and prices. Some of our standard categories include all made in Naija products, electronics, mobile phones, computers, fashion, beauty products, home and kitchen, Building and construction materials, automotive and industrial, books, musical equipment, babies’ and kids’ items, sports and fitness, and a whole lot more from the finest brands.
                </p>
                <p>
                    There are also added special services for the sellers and consumers, gift vouchers and discounts, in other to ensure the best and professional transaction across different categories to deliver to you an express and unforgettable online shopping experience.
                </p>
            </div>
            <div class="col-md-6">
                <h3 class="h1 heading-primary"><strong>Additional</strong> Benefits</h3>
                <p>
                    Also benefit from free shipping rates for certain products and locations as indicated on our sites and with the bulk purchase option, you can enjoy low shipping rates, discounted prices and flexible payment. Shopping on Goods2go Hub, you can pay with PayPal, your debit card, or pay on delivery; any of these at your very own choice and convenience.
                </p>
                <p>
                    Afford the best of your standard of living online right at the comfort of your home. This opportunity is here for you, hurry up now; shop and keep smiling as you will be getting the best prices on some of the best-selling and quality brands in Africa – what have you. Hence, An experience with Goods2go Hub online store is one that entails Shopping online with great ease and excellent speed.
                </p>
            </div>
        </div>
    </div>
@endsection