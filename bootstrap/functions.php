<?php

/**
 * generate link for breadcrumb
 * @param $segment
 * @param $key
 * @return string
 */
function segment_link($segment, $key) {
    $link = '/';
    for($i = 0; $i < $key+1; $i++) {
        $link .= $segment[$i];
    }
    return $link;
}


function generate_title() {
    $segments = request()->segments();
    if(!$segments) { // homepage
        return 'Home';
    } else {
        return ucwords(array_last($segments));
    }
}

function image($name) {
    return asset('/uploads/images/'.$name);
}

function image_path($name) {
    return '/uploads/images/'.$name;
}

function format_price($price) {
    return '₦'.number_format($price);
}

function include_image_link(&$goods) {
    $image_ids = $goods->map(function($good) {
        if($good->images) {
            return $good->images[0]; // the first image of each listing
        } else {
            return null;
        }
    })->all();

    if(!$image_ids) return;

    $images = \App\File::whereIn('id', array_unique($image_ids))->get();

    foreach ($goods as $item) {
        $item->setAttribute('image_link', $images->where('id', $item->images[0])->first()['link']);
    }
}


function plural($str, $no = 2) {
    return $no . ' ' . str_plural($str, $no);
}

function deleteImages($images) {
    $path = '/uploads/images';
    foreach ($images as $image) {
        Storage::delete([
            $path.'/original/'.$image->link,
            $path.'/lg/'.$image->link,
            $path.'/md/'.$image->link,
            $path.'/sm/'.$image->link
        ]);
    }
}



/**
 * views prefixed with admin
 *
 * @param $view
 * @param array $data
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */
function admin_view($view, $data = []) {
    return view('admin.'.$view, $data);
}

// route prefixed with admin
function admin_route($route, $options = []) {
    return route('admin.'.$route, $options);
}


function admin_active($text) {
    $segments = request()->segments();
    if(count($segments) == 1 && $text == 'dashboard')
        return 'active';

    foreach ($segments as $segment) {
        if($segment == 'admin')
            continue;
        if($text == $segment) {
            return 'active';
        }
    }
    return '';
}


function active($path) {
    $segments = request()->segments();
    if($segments[0] == $path)
        return 'active';
    return '';
}
